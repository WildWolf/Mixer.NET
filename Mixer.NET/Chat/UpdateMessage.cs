﻿using Newtonsoft.Json;

using System;

namespace Mixer.NET.Chat
{
    public class DeleteMessage : UpdateMessage
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        public override string Type => "DeleteMessage";
    }

    public class PurgeMessage : UpdateMessage
    {
        public override string Type => "PurgeMessage";

        [JsonProperty("user_id")]
        public uint UserId { get; set; }
    }

    public abstract class UpdateMessage
    {
        [JsonProperty("moderator")]
        public UpdateUserUser Moderator { get; set; }

        public abstract string Type { get; }
    }
}