﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;

namespace Mixer.NET.Chat
{
    [Flags]
    public enum MethodTag
    {
        [Description("auth")]
        Auth = 1 << 0,

        [Description("msg")]
        Message = 1 << 1,

        [Description("whisper")]
        Whisper = 1 << 2,

        [Description("vote:choose")]
        VoteChoose = 1 << 3,

        [Description("vote:start")]
        VoteStart = 1 << 4,

        [Description("timeout")]
        Timeout = 1 << 5,

        [Description("purge")]
        Purge = 1 << 6,

        [Description("deleteMessage")]
        DeleteMessage = 1 << 7,

        [Description("clearMessages")]
        ClearMessages = 1 << 8,

        [Description("history")]
        History = 1 << 9,

        [Description("giveaway:start")]
        Giveaway = 1 << 10,

        [Description("ping")]
        Ping = 1 << 11,

        [Description("attachEmotes")]
        AttachEmotes = 1 << 12,

        [Description("cancelSkill")]
        CancelSkill = 1 << 13,

        [Description("optOutEvents")]
        OptoutEvents = 1 << 14
    }

    internal static class MessageQueueExtension
    {
        public static string GetPacket(this MessageQueueClass message, int counter)
        {
            JObject ret = new JObject();
            ret.Add("type", "method");
            ret.Add("method", message.Method.GetType().GetMember(message.Method.GetType().GetEnumName(message.Method) ?? throw new InvalidOperationException())[0].GetCustomAttributes(typeof(DescriptionAttribute), true).Cast<DescriptionAttribute>().Select(f => f.Description).FirstOrDefault());
            ret.Add("arguments", JArray.FromObject(message.Args));
            message.LastId = counter;
            ret.Add("id", counter++);
            return ret.ToString(Formatting.None);
        }
    }

    internal class MessageQueue
    {
        private int counter = 0;
        private List<MessageQueueClass> messages = new List<MessageQueueClass>();

        public MessageQueueClass this[int index] => messages[index];

        public void Add(MessageQueueClass message)
        {
            messages.Add(message);
        }

        public MessageQueueClass GetNext()
        {
            if(!messages.Any()) throw new IndexOutOfRangeException();
            var message = messages.FirstOrDefault();
            Remove(message);
            Add(message);
            return message;
        }

        public KeyValuePair<MessageQueueClass, string> GetNextPacket()
        {
            var message = GetNext();
            return new KeyValuePair<MessageQueueClass, string>(message, message.GetPacket(counter));
        }

        public void Remove(MessageQueueClass message)
        {
            messages.Remove(message);
        }

        public void RemoveId(int id)
        {
            messages.Remove(messages.Where(t => t.LastId == id).FirstOrDefault());
        }
    }

    internal class MessageQueueClass
    {
        public MessageQueueClass(List<object> args, MethodTag method)
        {
            Args = args;
            Method = method;
        }

        public List<object> Args { get; }
        public int LastId { get; set; }
        public MethodTag Method { get; }
        public DateTime Timeout { get; set; }
    }
}