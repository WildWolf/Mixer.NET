﻿using Mixer.NET.Models;
using Newtonsoft.Json;
using System;

namespace Mixer.NET.Constellation.Models
{
    public class CResubscribed : CSubscribed
    {
        [JsonProperty("since")]
        public DateTime Since { get; set; }

        [JsonProperty("totalMonths")]
        public uint TotalMonths { get; set; }

        [JsonProperty("until")]
        public DateTime Until { get; set; }
    }

    public class CSubscribed
    {
        public string Type { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }
    }

    public class CSubscriptionGifted : CSubscribed
    {
        [JsonProperty("giftReceiverId")]
        public uint GiftReceiverId { get; set; }

        [JsonProperty("gifterId")]
        public uint GifterId { get; set; }

        [JsonProperty("giftReceiverUsername")]
        public string GiftReceiverUsername { get; set; }

        [JsonProperty("gifterUsername")]
        public string GifterUsername { get; set; }
    }

    public class UResubscribed : USubscribed
    {
        [JsonProperty("since")]
        public DateTime Since { get; set; }

        [JsonProperty("totalMonths")]
        public uint TotalMonths { get; set; }

        [JsonProperty("until")]
        public DateTime Until { get; set; }
    }

    public class USubscribed
    {
        [JsonProperty("channel")]
        public uint ChannelId { get; set; }

        public string Type { get; set; }
    }

    public class USubscriptionGifted : USubscribed
    {
        [JsonProperty("giftReceiverId")]
        public uint GiftReceiverId { get; set; }

        [JsonProperty("channelId")]
        public uint ChannelId { get; set; }

        [JsonProperty("giftReceiverUsername")]
        public string GiftReceiverUsername { get; set; }
    }
}