﻿using Mixer.NET.Models;

using Newtonsoft.Json;

namespace Mixer.NET.Constellation.Models
{
    public class CFUpdate
    {
        [JsonProperty("following")]
        public bool Following { get; set; }

        [JsonProperty("user")]
        public UserWithChannel User { get; set; }
    }

    public class UFUpdate
    {
        [JsonProperty("channel")]
        public Channel Channel { get; set; }

        [JsonProperty("following")]
        public bool Following { get; set; }
    }
}