﻿using Newtonsoft.Json;

namespace Mixer.NET.Models
{
    public class Analytic : IJsonSerializable
    {
        public CPMAnalytic CpmAnalytic { get; set; }
        public FollowersAnalytic FollowersAnalytic { get; set; }
        public GameRankAnalytic GameRankAnalytic { get; set; }
        public StreamHostsAnalytic StreamHostsAnalytic { get; set; }
        public StreamSessionsAnalytic StreamSessionsAnalytic { get; set; }
        public SubRevenueAnalytic SubRevenueAnalytic { get; set; }
        public SubscriptionsAnalytic SubscriptionsAnalytic { get; set; }
        public ViewerAnalytic ViewerAnalytic { get; set; }
        public ViewerMetricAnalytic ViewerMetricAnalytic { get; set; }
        public ViewerSessionCountAnalytic ViewerSessionCountAnalytic { get; set; }
    }

    public class Achivement : IJsonSerializable
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("sparks")]
        public uint Sparks { get; set; }
    }

    public class AchivementEarning : TimeStamped, IJsonSerializable
    {
        [JsonProperty("Achivement")]
        public Achivement Achivement { get; set; }

        [JsonProperty("earned")]
        public bool Earned { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("achivement")]
        public string Name { get; set; }

        [JsonProperty("progress")]
        public double Progress { get; set; }

        [JsonProperty("user")]
        public uint User { get; set; }
    }

    public class Announcement : IJsonSerializable
    {
        [JsonProperty("level")]
        public string Level { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("sound")]
        public string Sound { get; set; }

        [JsonProperty("timeout")]
        public int Timeout { get; set; }
    }
}