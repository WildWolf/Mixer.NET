﻿using Newtonsoft.Json;
using System;

namespace Mixer.NET.Models
{
    public class VideoManifestResolution : IJsonSerializable
    {
        [JsonProperty("bitrate")]
        public uint Bitrate { get; set; }

        [JsonProperty("hasVideo")]
        public bool HasVideo { get; set; }

        [JsonProperty("height")]
        public uint? Height { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("width")]
        public uint? Width { get; set; }
    }

    public class VideoSettings : IJsonSerializable
    {
        [JsonProperty("isLightstreamEnabled")]
        public bool IsLightStreamEnabled { get; set; }
    }

    public class ViewerAnalytic : ChannelAnalytic
    {
        [JsonProperty("anon")]
        public uint Anon { get; set; }

        [JsonProperty("authed")]
        public uint Authed { get; set; }
    }

    public class ViewerCount : IJsonSerializable
    {
        [JsonProperty("anon")]
        public uint Anon { get; set; }

        [JsonProperty("authed")]
        public uint Authed { get; set; }

        [JsonProperty("time")]
        public DateTime Time { get; set; }
    }

    public class ViewerMetricAnalytic : ChannelAnalytic
    {
        [JsonProperty("browser")]
        public string Browser { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("platform")]
        public string Platform { get; set; }
    }

    public class ViewerSessionCountAnalytic : ChannelAnalytic
    {
        [JsonProperty("count")]
        public uint Count { get; set; }
    }

    public class VOD : TimeStamped
    {
        [JsonProperty("baseUrl")]
        public string BaseUrl { get; set; }

        [JsonProperty("data")]
        public VODData Data { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("recordingId")]
        public uint RecordingId { get; set; }
    }

    public class VODData : IJsonSerializable
    {
        [JsonProperty("Bitrate")]
        public uint? Bitrate { get; set; }

        [JsonProperty("Fps")]
        public decimal Fps { get; set; }

        [JsonProperty("Height")]
        public uint Height { get; set; }

        [JsonProperty("Width")]
        public uint Width { get; set; }
    }
}