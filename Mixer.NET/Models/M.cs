﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mixer.NET.Models
{
    public enum Maturity
    {
        Family = 1,
        Teen = 2,
        EighteenPlus = 3
    }
}
