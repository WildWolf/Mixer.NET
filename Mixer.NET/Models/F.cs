﻿using Newtonsoft.Json;
using System;

namespace Mixer.NET.Models
{
    public class Follow : TimeStamped
    {
        [JsonProperty("channel")]
        public uint Channel { get; set; }

        [JsonProperty("user")]
        public uint User { get; set; }
    }

    public class FollowersAnalytic : ChannelAnalytic
    {
        [JsonProperty("delta")]
        public int Delta { get; set; }

        [JsonProperty("total")]
        public uint Total { get; set; }

        [JsonProperty("user")]
        public int User { get; set; }
    }

    public class FrontendVersion : IJsonSerializable
    {
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }
    }

    public class FTLVideoManifest : IJsonSerializable
    {
        [JsonProperty("resolutions")]
        public VideoManifestResolution[] Resolutions { get; set; }

        [JsonProperty("since")]
        public DateTime Since { get; set; }
    }
}