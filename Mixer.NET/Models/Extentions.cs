﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mixer.NET.Models
{
    public static class RolesExtension
    {
        public static bool IsMod(this Role roles)
        {
            return roles.HasFlag(Role.Mod) || roles.HasFlag(Role.Owner) || roles.HasFlag(Role.Staff) || roles.HasFlag(Role.GlobalMod) || roles.HasFlag(Role.Founder);
        }

        public static bool IsSub(this Role roles)
        {
            return roles.HasFlag(Role.Subscriber);
        }

        public static bool IsEditor(this Role roles)
        {
            return roles.HasFlag(Role.ChannelEditor) || roles.HasFlag(Role.Owner) || roles.HasFlag(Role.Staff) || roles.HasFlag(Role.Founder);
        }

        public static bool IsPro(this Role roles)
        {
            return roles.HasFlag(Role.Pro);
        }

        public static bool IsSubAndHigher(this Role roles)
        {
            return roles.IsSub() || roles.IsMod();
        }

        public static bool IsProAndHigher(this Role roles)
        {
            return roles.IsPro() || roles.IsSub() || roles.IsMod();
        }
    }

    public static class UserExtensions
    {
        public static Role GetAllRoles(this IEnumerable<UserGroup> groups)
        {
            return (Role)groups.Select(g => (int)g.Role).Sum();
        }
    }
}
