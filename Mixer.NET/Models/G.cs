﻿using Newtonsoft.Json;

namespace Mixer.NET.Models
{
    public class GameRankAnalytic : ChannelAnalytic
    {
        [JsonProperty("shared")]
        public uint Shared { get; set; }

        [JsonProperty("streams")]
        public uint Streams { get; set; }

        [JsonProperty("views")]
        public uint Views { get; set; }
    }

    public class GameType : GameTypeSimple
    {
        [JsonProperty("description")]
        public string Dascription { get; set; }

        [JsonProperty("online")]
        public uint Online { get; set; }

        [JsonProperty("parent")]
        public string Parent { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("viewersCurrent")]
        public uint ViewersCurrent { get; set; }
    }

    public class GameTypeLookup : GameTypeSimple
    {
        [JsonProperty("exact")]
        public uint Exact { get; set; }
    }

    public class GameTypeSimple : IJsonSerializable
    {
        [JsonProperty("backgroundUrl")]
        public string BackgroundUrl { get; set; }

        [JsonProperty("coverUrl")]
        public string CoverUrl { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}