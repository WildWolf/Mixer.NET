﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public class Session : IJsonSerializable
    {
        [JsonProperty("expires")]
        public uint Expires { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("meta")]
        public SessionMeta Meta { get; set; }
    }

    public class SessionMeta : IJsonSerializable
    {
        [JsonProperty("client")]
        public string Client { get; set; }

        [JsonProperty("cversion")]
        public string CVersion { get; set; }

        [JsonProperty("device")]
        public string Device { get; set; }

        [JsonProperty("os")]
        public string OS { get; set; }
    }

    public class Share : TimeStamped
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("relid")]
        public string RelId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("userId")]
        public uint? UserId { get; set; }
    }

    public class SocialInfo : IJsonSerializable
    {
        [JsonProperty("discord")]
        public string Discord { get; set; }

        [JsonProperty("facebook")]
        public string Facebook { get; set; }

        [JsonProperty("player")]
        public string Player { get; set; }

        [JsonProperty("twitter")]
        public string Twitter { get; set; }

        [JsonProperty("verified")]
        public List<string> Verified { get; set; }

        [JsonProperty("youtube")]
        public string YouTube { get; set; }
    }

    public class StreamHostsAnalytic : ChannelAnalytic
    {
        [JsonProperty("hostee")]
        public uint? Hostee { get; set; }

        [JsonProperty("hoster")]
        public uint? Hoster { get; set; }
    }

    public class StreamSessionsAnalytic : ChannelAnalytic
    {
        [JsonProperty("duration")]
        public uint? Duration { get; set; }

        [JsonProperty("online")]
        public bool Online { get; set; }

        [JsonProperty("partnered")]
        public bool Partnered { get; set; }

        [JsonProperty("type")]
        public uint? Type { get; set; }
    }

    public class StreamViewer : IJsonSerializable
    {
        [JsonProperty("broadcastId")]
        public string BroadcastId { get; set; }

        [JsonProperty("browser")]
        public string Browser { get; set; }

        [JsonProperty("channelId")]
        public string ChannelId { get; set; }

        [JsonProperty("costreamDepth")]
        public string CostreamDepth { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("durationInSeconds")]
        public decimal DurationInSeconds { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("partnered")]
        public bool Partnered { get; set; }

        [JsonProperty("platform")]
        public string Platform { get; set; }

        [JsonProperty("startedAt")]
        public DateTime StartedAt { get; set; }

        [JsonProperty("typeId")]
        public string TypeId { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }
    }

    public class SubRevenueAnalytic : ChannelAnalytic
    {
        [JsonProperty("count")]
        public uint Count { get; set; }

        [JsonProperty("gateway")]
        public string Gateway { get; set; }

        [JsonProperty("gross")]
        public decimal Gross { get; set; }

        [JsonProperty("total")]
        public decimal Total { get; set; }
    }

    public class Subscription : TimeStamped
    {
        [JsonProperty("cancelled")]
        public bool Cancelled { get; set; }

        [JsonProperty("expiresAt")]
        public DateTime ExpiresAt { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("resourceId")]
        public uint? ResourceId { get; set; }

        [JsonProperty("resourceType")]
        public string ResourceType { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public class SubscriptionExtended : Subscription
    {
        [JsonProperty("group")]
        public UserGroup Group { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }
    }

    public class SubscriptionPlan : IJsonSerializable
    {
        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class SubscriptionsAnalytic : ChannelAnalytic
    {
        [JsonProperty("delta")]
        public decimal Delta { get; set; }

        [JsonProperty("total")]
        public uint Total { get; set; }

        [JsonProperty("user")]
        public uint User { get; set; }
    }

    public class SubscriptionWithGroup : Subscription
    {
        [JsonProperty("Group")]
        public UserGroup Group { get; set; }

        [JsonProperty("group")]
        public uint GroupId { get; set; }
    }
}