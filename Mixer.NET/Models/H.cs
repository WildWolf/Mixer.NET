﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public class HostingSession : IJsonSerializable
    {
        [JsonProperty("hostees")]
        public List<uint> Hostees { get; set; }

        [JsonProperty("layout")]
        public PlayerLayout Layout { get; set; }
    }
}